﻿using DAL;
using BLL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UTIL;


namespace DemoMVCApp.Controllers
{
    public class ProductController : Controller
    {

        Logger logger = Logger.GetInstance();

        // GET: Proizvodi
        public ActionResult Index()
        {
            try
            {
                ProductsService products = new ProductsService();
                List<Product> prods = products.ListOfProducts();
                return View(prods);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
            
            return View("~/Views/Shared/Error.cshtml");
        }

        public ActionResult ListOfProducts()
        {
            try
            {
                ProductsService products = new ProductsService();
                List<Product> prods = products.ListOfProducts();
                return View(prods);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return View("~/Views/Shared/Error.cshtml");
        }


        public ActionResult NewProduct()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewProduct(Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (BaseEntities context = new BaseEntities())
                    {
                        context.Products.Add(product);
                        context.SaveChanges();
                        logger.Info("Successfully added record in the database regarding Products");
                        return RedirectToAction("Index", "Product");
                    }
                }
            }
            catch (Exception e)
            {
                
                logger.Error(e.Message);
                ModelState.AddModelError("Product", e.Message);
                
            }

            return View("~/Views/Shared/Error.cshtml");
        }
        
        // GET Products/Edit/1
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return HttpNotFound();
            }
            try
            {
                Product product = new Product();
                if (ModelState.IsValid)
                {

                    using (BaseEntities context = new BaseEntities())
                    {
                        product = context.Products.SingleOrDefault(e => e.ProductId == id);
                        if (product == null || id == 0)
                        {
                            return HttpNotFound();
                        }
                        logger.Info("Product found and can be edited");   
                    }
                }

                return View(product);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.Message);
                ModelState.AddModelError("Product",e.Message);
               
            }

            return View("~/Views/Shared/Error.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveChanges([Bind(Include = "ProductId, Name, Description, Category, Brand, Supplier, Price")]Product products)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    using (BaseEntities context = new BaseEntities())
                    {
                        context.Entry(products).State = EntityState.Modified;
                        context.SaveChanges();
                        logger.Info("Successfully edited record in the database regarding Products");
                        return RedirectToAction("Index", "Product");

                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                ModelState.AddModelError("Product", e.Message);
                
            }

            return View("~/Views/Shared/Error.cshtml");
        }

    }
}