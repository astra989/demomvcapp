﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL;

namespace DemoMVCApp.Controllers
{
    public class ProductJSONController : Controller
    {
        Logger logger = Logger.GetInstance();

        // GET: ProductJSON
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListOfProductsJSON()
        {
            try
            {
                ProductsService products = new ProductsService();
                List<Product> prods = products.ListOfProductsJSON();
                return View(prods);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return View("~/Views/Shared/Error.cshtml");
        }
    }
}