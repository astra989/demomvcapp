﻿using DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using UTIL;

namespace BLL
{
    public class ProductsService 
    {
        Logger logger = Logger.GetInstance();

        public List<Product> ListOfProducts()
        {
            try
            {
                List<Product> products;
                using (BaseEntities context = new BaseEntities())
                {
                    products = context.Products.ToList();
                }

                return products;
            }
            catch (SqlException e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        //getting data from JSON file
        public List<Product> ListOfProductsJSON()
        {
            try
            {
                
                List<Product> products;
                String path = HttpContext.Current.Server.MapPath("~/JsonData/Products.json");
                using (StreamReader sr = new StreamReader(path))
                {
                    products = JsonConvert.DeserializeObject<List<Product>>(sr.ReadToEnd());
                }
                logger.Info("Successfully deserialized Product file");
                return products;
            }
            catch (FileNotFoundException e)
            {
                logger.Error(e.Message);

            }

            return null;
        }

        

       
    }
}
