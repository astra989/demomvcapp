﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace UTIL
{
    public class Logger
    {
        private static Logger instance;

        private Logger()
        {
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance  = new Logger();
            }

            return instance;
        }

        private static void WriteToAFile(string message)
        {
           
            if (message != null)
            {
                String path = HttpContext.Current.Server.MapPath("~/LogFile.txt");
                using (StreamWriter stream = new StreamWriter(path, append:true))
                {
                    stream.Write(DateTime.Now + ":" + message);
                }
            }
        }

        public void Info(string message)
        {
            if (message != null)
            {
                Console.WriteLine("Info: " + message);
                WriteToAFile("Info: " + message);
            }
        }

        public void Error(string message)
        {
            if (message != null)
            {
                Console.WriteLine("Error: " + message);
                WriteToAFile("Error: " + message);
            }

        }

        public void Debug(string message)
        {
            if (message != null)
            {
                Console.WriteLine("Debug: " + message);
                WriteToAFile("Debug: " + message);
            }

            
        }
    }
}
